# Projet de Génie Logiciel

Ce projet est réalisé dans le cadre du cours de Génie Logiciel à l'ENSAI. Nous proposons un service de gestion de vidéoclub, qui permet à un administrateur d'effectuer quelques opérations courantes pour son activité. Il est réalisé en python. Il comporte une base de données SQLite qui stocke les informations nécessaires au fonctionnement du vidéoclub, un serveur qui expose une API afin de communiquer avec la base de données et enfin un client qui permet un accès simplifié à l'outil.

Pour les besoins de l'application, il est préférable que vous possédiez un token de l'API IMDB. Cela permet d'avoir le meilleur catalogue qui soit: les 250 meilleurs films actuellement. Pour créer un token, cliquez [ici](https://imdb-api.com/api/#Top250Movies-header).

Pour tester l'application avec le client, il faut s'authentifier. Pour l'instant, vous pouvez utiliser un des utilisateurs enregistrés à l'initialisation de la base de données.


## Quickstart - sans Docker

Pour cloner le projet et installer les dépendances:
```
git clone https://gitlab.com/projet-gl1/imdb.git
cd imdb
pip install -r client/requirements.txt
pip install -r app/requirements.txt
```

Pour créer la base de données:
```
cd app
sqlite3 imdb.db  < initdata.sql
```

Pour lancer les test unitaires:
```
python3 -m unittest
```

Pour initialiser le catalogue de films disponibles:
- Option 1 (recommandée): utiliser l'API IMDB, afin d'obtenir le catalogue le plus à jour possible, où XXXX correspond à votre token IMDB
```
python3 apiImdb.py --token XXXX
```
- Option 2: si vous ne souhaitez pas créer de compte auprès de l'API IMDB et qu'obtenir un catalogue pas tout à fait à jour ne vous dérange pas
```
python3 loadFilms.py
```

Pour la suite, vous devez créer une clé secrète JWT et la sauvegarder dans le fichier .env, sous la clé 'KEYFLASK'.
Ensuite, pour lancer le serveur:
```
python3 main.py
```

Pour lancer le client, dans un nouveau terminal:
```
cd client
python3 main.py
```

## Quickstart - avec Docker

```
git clone https://gitlab.com/projet-gl1/imdb.git
cd imdb
```

Les XXXX ci-dessous sont à remplacer par votre token IMDB
```
docker-compose build --build-arg token=XXXX
docker-compose up
docker-compose ps
```

Dans un autre terminal, pour lancer les tests:
```
docker exec -it imdb_serveur_1 bash
python -m unittest
```

Dans un nouveau terminal, pour lancer le client:
```
python3 client/main.py 
```


## Fonctionnement du code

Côté base de données, notre projet nécessite les tables suivantes:
- users: utilisateurs de l'application, entrés à la création de la base de données
- films: films présents dans le catalogue du vidéoclub
- customers: clients du vidéoclub, qui peuvent réaliser des emprunts
- reservations: registre des emprunts effectués par les clients

Nous utilisons une base de données SQLite, contenant ces tables et stockée au niveau du serveur.

Côté serveur, le fichier app/main.py implémente une API via Flask. Les différentes endpoints exposés sont les suivants:
- /login: permet de gérer l'authentification nécessaire à l'accès de tous les endpoints suivants
- /films: permet de récupérer le contenu de la table films, avec des options de filtrage par année, titre et note
- /reserve: permet d'enregistrer une réservation de film pour un client
- /return: permet d'enregistrer un retour de film par un client
- /UPDATE: permet de modifier la note ou le classement d'un film présent dans le catalogue
- /DELETE: permet de supprimer un film du catalogue
- /newcustomer: permet d'ajouter un nouveau client dans la table client

Les relations entre la base de données et l'application Flask sont gérées par des DAO - Data Access Objects.

Côté client, un client simple est implémenté dans le fichier client/main.py. Il permet, une fois authentifié, d'utiliser les principales fonctionnalités de l'application créée qui sont proposées sous forme de liste d'options. Les fonctions qui permettent de réaliser les actions demandées par l'utilisateur de notre application s'occupent simplement d'effectuer la bonne requête à notre API, d'en récupérer le résultat, et d'afficher un message d'information ou d'erreur selon le statut de la requête. 

## Contributrices

- Mariem Ben Hamouda : mariem.ben-hamouda@eleve.ensai.fr
- Zixuan Wang : zixuan.wang@eleve.ensai.fr
- Maëlle Cosson : maelle.cosson@eleve.ensai.fr
import requests
import pandas as pd
import click
import sqlite3

# Send GET request to the API endpoint
@click.command()
@click.option('--token')
def run(token):

    # API endpoint
    url = f"https://imdb-api.com/en/API/Top250Movies/{token}"
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    df = pd.DataFrame(data["items"])
    l=[1 for i in range(len(df))]
    df['available']=l
    #Connect to the database
    conn = sqlite3.connect('imdb.db')

    # Create a cursor
    c = conn.cursor()
   
    # Insert the movie data into the table
    for i, row in df.iterrows():
        c.execute("INSERT INTO films VALUES (?,?,?,?,?,?,?)", (row["id"],row["rank"], row["title"], row["year"], row["imDbRating"], row["crew"],row["available"]))

    # Commit the changes and close the connection
    conn.commit()
    conn.close()


if __name__=='__main__':
    run()




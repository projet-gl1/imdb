from dao.userdao import *

class Authenticate:

    # Define a function to handle user authentication
    def authenticate(self,username, password):
        user_dao = UserDao()
        user=user_dao.find_user_by_username(username)
        if user and user.mot_de_passe== password:
        #return user if authenticated
            return {'username': username}
        else:
            return None

    # Define a function to handle user identity
    def identity(self,payload):
        # Retrieve user information from the payload
        return {'username': payload['identity']}
from typing import List
from utils.singleton import Singleton
from dao.customer import customer
import sqlite3

class CustomerDao(metaclass=Singleton):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)

    def find_all_customers(self) -> List[customer]:
        cursor = self.conn.execute("SELECT * FROM customers")
        customers = cursor.fetchall()
        return [customer(*row) for row in customers]
        
    def get_all_customer_names(self) -> List[str]:
        customers = self.find_all_customers()
        return [u.customer_name for u in customers]
   
    def find_customer_by_id(self, id:int)-> customer:
        cursor=self.conn.execute('''SELECT * FROM customers WHERE id_customer=?''',(id,))
        row = cursor.fetchone()
        return customer(*row) if row else None

    def find_customer_by_name(self, name: str) -> customer:
        cursor = self.conn.execute("SELECT * FROM customers WHERE customer_name=?", (name,))
        row = cursor.fetchone()
        return customer(*row) if row else None

    def add_customer(self, cust: customer) -> bool:
        cursor = self.conn.execute("INSERT INTO customers (customer_name) VALUES (?)",
                                   (cust.customer_name,))
        self.conn.commit()
        customer.id_customer = cursor.lastrowid
        return cursor.rowcount > 0

    def update_user(self, cust: customer) -> bool:
        cursor = self.conn.execute("UPDATE customers SET customer_name=?",
                                   (cust.customer_name))
        self.conn.commit()
        return cursor.rowcount > 0

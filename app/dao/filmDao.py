import sqlite3
from pattern.filterByTitle import *
from pattern.filterByYear import *
from pattern.filterByRating import *
from utils.singleton import *
from dao.reservation import *


class filmDao(metaclass=Singleton):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)

    def get_films(self,filter_strategy: filterStrategy = None):
        cursor =self.conn.execute(
        'SELECT * FROM films where available=1'
        )
        # Extraction des résultats de la requête
        films = [{'id': row[0], 'title': row[2], 'year': row[3],'rating': row[4],'crew': row[5],'available':row[6]} for row in cursor]
        if filter_strategy:
            # Appliquer le filtrage avec la stratégie sélectionnée
            films = filter_strategy.filter(films)
        if films:
            # Renvoi des films trouvés
            return films
        else:
            return None
    
    def find_film_by_title(self, title: str):
        filter_strategy1=filterByTitle(title)
        return self.get_films(filter_strategy1)
    
    def find_films_by_year(self, Year: str):
        filter_strategy1=filterByYear(Year)
        return self.get_films(filter_strategy1)

    def find_films_by_rating(self, rating: float):
        filter_strategy1=filterByRating(rating)
        return self.get_films(filter_strategy1)

    def delete_film(self, film_id: str):
        delete=False
        try:
            cursor = self.conn.cursor()
            query = '''DELETE FROM films WHERE id = ? '''
            cursor.execute(query, (film_id,))
            self.conn.commit()
            delete=True
            return delete
        except:
            return delete

    def update_film(self,film_id: str,rating: float,rank: int):
        update=False
        try:
            cursor = self.conn.cursor()
            query = '''UPDATE films SET rank=?, rating=? WHERE id = ? '''
            cursor.execute(query, (rank,rating,film_id))
            self.conn.commit()
            update=True
            return update
        except:
            return update
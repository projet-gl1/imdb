import sqlite3

from utils.singleton import *


class reservationDao(metaclass=Singleton):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)
    #conn.execute("PRAGMA foreign_keys = ON")

    def reserve_film(self, customer_id, film_id):
        reversed=False
        cursor =self.conn.execute("SELECT * FROM films WHERE id = ? and available = 1", (film_id,))
        film = cursor.fetchone()
        if film:
            self.conn.execute(
                "UPDATE films SET available = 0 WHERE id = ?", (film_id,)
            )
            try:

                self.conn.execute(
                    '''
                        INSERT INTO reservations (customer_id, film_id, reservation_date) 
                        VALUES (?, ?, datetime('now'))
                        ''', (customer_id, film_id)
                )
                self.conn.commit()
                reversed=True
                return reversed
            except:
                print("The customer does not exist.")
                return reversed
        else:
            print("The film is not available or does not exist.")
            return reversed

    def return_film(self,customer_id,film_id):
        returned=False
        try:
            self.conn.execute('''UPDATE films SET available = 1 WHERE id = ?''', (film_id,))
            self.conn.execute('''UPDATE reservations SET end_date =datetime('now') WHERE customer_id = ? AND film_id = ?''', (customer_id,film_id ))
            self.conn.commit()
            returned=True
            return returned
        except:
            return returned

    def check_id_film_exists(self,film_id):
        cursor = self.conn.cursor()
        query = '''SELECT * FROM films WHERE id = ? '''
        cursor.execute(query, (film_id,))
        result = cursor.fetchone()
        return result is not None

    def check_if_film_reserved_by_customer(self,customer_id, movie_id):
        cursor = self.conn.cursor()
        query = '''SELECT * FROM reservations WHERE customer_id = ? AND film_id = ? '''
        cursor.execute(query, (customer_id, movie_id))
        result = cursor.fetchone()
        return result is not None

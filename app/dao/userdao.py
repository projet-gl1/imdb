from typing import List
from utils.singleton import Singleton
from dao.user import user
import sqlite3

class UserDao(metaclass=Singleton):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)

    def find_all_users(self) -> List[user]:
        cursor = self.conn.execute("SELECT * FROM users")
        users = cursor.fetchall()
        return [user(*row) for row in users]
        
    def get_all_user_names(self) -> List[str]:
        users = self.find_all_users()
        return [u.name for u in users]
   
    def find_user_by_id(self,id:int)-> user:
        cursor=self.conn.execute('''SELECT * from users WHERE id_user=?''',(id,))
        row = cursor.fetchone()
       
        return user(*row) if row else None

    def find_user_by_username(self, username: str) -> user:
        cursor = self.conn.execute("SELECT * FROM users WHERE user_name=?", (username,))
        row = cursor.fetchone()
        return user(*row) if row else None

    def add_user(self, user: user) -> bool:
        cursor = self.conn.execute("INSERT INTO users (user_name, email, qst_mystère, mot_de_passe) VALUES (?,?,?,?)",
                                   (user.name, user.email, user.qst_mystère, user.mot_de_passe))
        self.conn.commit()
        user.id_user = cursor.lastrowid
        return cursor.rowcount > 0

PRAGMA foreign_keys = ON;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id_user INTEGER PRIMARY KEY AUTOINCREMENT,
    user_name  TEXT UNIQUE,
    email  TEXT UNIQUE,
    qst_mystère TEXT,
    mot_de_passe TEXT);

INSERT INTO users (user_name, email, qst_mystère, mot_de_passe) VALUES
    ('maelle', 'maelle.cosson@eleve.ensai.fr', 'chat', '1234'),
    ('maryem', 'mariem.ben-hamouda@eleve.ensai.fr', 'chien', '12345');

/*select * from USERS;*/
DROP TABLE IF EXISTS films ;
CREATE TABLE films (
    id TEXT,
    rank NUMERIC,
    title TEXT,
    year TEXT, 
    rating REAL,
    crew TEXT,
    available NUMERIC,
    PRIMARY KEY (id));



DROP TABLE IF EXISTS customers;
CREATE TABLE customers (
    id_customer INTEGER PRIMARY KEY AUTOINCREMENT,
    customer_name  TEXT UNIQUE);



DROP TABLE IF EXISTS reservations;
CREATE TABLE reservations (
    id_res INTEGER PRIMARY KEY AUTOINCREMENT,
    customer_id NUMERIC,
    film_id TEXT,
    reservation_date DATE,
    end_date DATE DEFAULT "not yet returned",
    FOREIGN KEY (customer_id) REFERENCES customers(id_customer),
    FOREIGN KEY (film_id) REFERENCES films(id));


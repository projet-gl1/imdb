import requests
import csv
import sqlite3


'''
Ce script peut être utilisé si vous ne souhaitez pas créer de compte auprès de l'API IMDB, afin de récupérer le classement à jour
des 250 films les plus populaires, qui constituent le catalogue du vidéoclub. 
'''

def run(filepath):

    conn = sqlite3.connect('imdb.db')
    c = conn.cursor()

    with open(filepath, 'r') as f:
        csvreader = csv.DictReader(f)

        for row in csvreader:
            c.execute("INSERT INTO films VALUES (?,?,?,?,?,?,?)", (row["id"], row["rank"], row["title"], row["year"], row["imDbRating"], row["crew"], 1))

    conn.commit()
    conn.close()


if __name__=='__main__':
    run('top250films.csv')

import sys
sys.path.insert(1, '.')

from pattern.filterByTitle import *
from pattern.filterByYear import *
from dao.filmDao import *
from dao.reservation import *
from dao.authenticate import *
from dao.customerDao import *
from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, jwt_required,create_access_token
from dotenv import load_dotenv
import os
import http.server

load_dotenv()

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = os.environ.get('KEYFLASK')
jwt = JWTManager(app)
handler = http.server.SimpleHTTPRequestHandler

@app.get("/")
def root():
    return {"hello":"world"}

# Define the endpoint for user login
@app.route('/login', methods=['POST'])
def login():
  # Retrieve the user credentials from the request
  username = request.json.get('username', None)
  password = request.json.get('password', None)

  # Authenticate the user
  Authenticate1=Authenticate()
  user =Authenticate1.authenticate(username, password)
  if user is None:
    # Return a 401 Unauthorized error if authentication fails
    return jsonify({'message': 'Unauthorized'}), 401

  # Create a JWT access token for the authenticated user
  access_token = create_access_token(identity=user['username'])
  return jsonify({'access_token': access_token}), 200

@app.route('/films', methods=['GET'])
@jwt_required()
def get_film():
    title = request.args.get('title')
    year = request.args.get('year')
    rating=request.args.get('rating')
    filmDao1 = filmDao()

    filter_strategy = None
    if title:
        res = filmDao1.find_film_by_title(title)
        if res:
            filter_strategy = filterByTitle(title)
        else:
            return f"Le film {title} n'existe pas", 200
        
    elif year:
        res = filmDao1.find_films_by_year(year)
        if res:
            filter_strategy = filterByYear(year)
        else:
            return "Aucun film à cette année", 200
    elif rating:
        rating=float(rating)
        res = filmDao1.find_films_by_rating(rating)
        if res:
            filter_strategy = filterByRating(rating)
        else:
            return "Aucun film n'a obtenu cette note", 200
  
    films = filmDao1.get_films(filter_strategy)
    if films:
        return jsonify(films)
    else:
        return "Aucun film trouvé", 200

@app.route('/reserve', methods=['POST'])
@jwt_required()
def reserve_movie():
    customer_name = request.json['customer_name']
    movie_id = request.json['movie_id']
    reservation=reservationDao()
    c=CustomerDao()
    # Check if the customer exists
    c_exists = c.find_customer_by_name(customer_name)
    #print(c_exists)
    if not c_exists:
        return "Client non trouvé", 404
    customer_id = c_exists.id_customer
    
    # Check if the movie exists
    movie_exists =reservation.check_id_film_exists(movie_id)
    if not movie_exists:
        return "Film non trouvé", 404

    # Reserve the movie
    result = reservation.reserve_film(customer_id, movie_id)
    if result:
        return "Film réservé avec succès", 200
    else:
        return "Echec de la réservation,le film n'est pas disponible", 400

@app.route('/return', methods=['POST'])
@jwt_required()
def return_movie():
    customer_name = request.json['customer_name']
    movie_id = request.json['movie_id']
    reservation=reservationDao()
    c=CustomerDao()

    # Check if the user exists
    c_exists = c.find_customer_by_name(customer_name)
    if not c_exists:
     return "Client non trouvé", 404
    customer_id = c_exists.id_customer
     
    # Check if the movie exists
    movie_exists =reservation.check_id_film_exists(movie_id)
    if not movie_exists:
        return "Film non trouvé", 404

    # Check if the movie is already reserved by the user
    reserved = reservation.check_if_film_reserved_by_customer(customer_id, movie_id)
    if not reserved:
        return "Le film n'a pas été réservé par le client", 400

    # Return the movie
    result = reservation.return_film(customer_id, movie_id)
    if result:
        return "Film rendu avec succès", 200
    else:
        return "Echec du retour du film", 400


@app.route('/UPDATE', methods=['PUT'])
@jwt_required()
def update_movie():
    movie_id = request.json['movie_id']
    rating=request.json['rating']
    rank=request.json['rank']
    reservation = reservationDao()
    movie_exists = reservation.check_id_film_exists(movie_id)   
    if not movie_exists :
     return "Film non trouvé", 404
    # update the movie
    film=filmDao()
    result=film.update_film(movie_id,rating,rank)
    if result:
        return "Film modifié avec succès", 200
    else:
        return "Echec de modifier le film", 400


@app.route('/DELETE', methods=['DELETE'])
@jwt_required()
def delete_movie():
    movie_id = request.json['movie_id']
    reservation = reservationDao()
    movie_exists = reservation.check_id_film_exists(movie_id)   
    if not movie_exists :
     return "Film non trouvé", 404
    # delete the movie
    film=filmDao()
    result=film.delete_film(movie_id)
    if result:
        return "Film supprimé avec succès", 200
    else:
        return "Echec de supprimer le film", 400


@app.route('/newcustomer', methods=['POST'])
@jwt_required()
def ajout_client():
    customer_name= request.json['customer_name']
    Customer = CustomerDao()
    Customer_exists =Customer.find_customer_by_name(customer_name) 
    if Customer_exists :
        return "Le client existe déja", 404
        # ajout le client t
    c=customer(customer_name)
    print(c.customer_name)
    result=Customer.add_customer(c)
    if result:
        return "Client inscrit avec succès", 200
    else:
        return "Echec d'inscription ", 400


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port = '5002')

from pattern.filterStrategy import filterStrategy


class filterByRating(filterStrategy):

  def __init__(self, Rating: float):
    self._rating = Rating

  def filter(self, films):
    # Filtrage des films par année
    return [film for film in films if film['rating'] >= self._rating]
from pattern.filterStrategy import filterStrategy
from typing import Text


class filterByTitle(filterStrategy):
  
  def __init__(self, title:Text):
    self._title = title

  def filter(self, films):
    # Filtrage des films par titre
    return [film for film in films if film['title'] == self._title]

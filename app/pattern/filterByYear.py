from pattern.filterStrategy import filterStrategy
from typing import Text


class filterByYear(filterStrategy):

  def __init__(self, year:Text):
    self._year = year

  def filter(self, films):
    # Filtrage des films par année
    return [film for film in films if film['year'] == self._year]
from abc import ABC, abstractmethod

class filterStrategy(ABC):
    @abstractmethod
    def filter(self, films):
        pass

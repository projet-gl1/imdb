import unittest
from dao.authenticate  import *
from dao.userdao import *

class TestAuthentication(unittest.TestCase):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)
    
    def test01_authenticate_with_valid_credentials(self):
        query=''' INSERT INTO users (user_name, email, qst_mystère, mot_de_passe)VALUES ('user_test', 'test@email.com', 'qst_test', 'mdp_test') '''
        cursor =self.conn.cursor()
        cursor.execute(query)
        self.conn.commit()   

        test_username = 'user_test'
        test_password_ok = 'mdp_test'
        test_password_nok="mdp"
        auth_service = Authenticate()
        result = auth_service.authenticate(username=test_username, password=test_password_ok)
        self.assertIsNotNone(result)
        self.assertEqual(result['username'], test_username)
        result = auth_service.authenticate(username=test_username, password=test_password_nok)
        self.assertIsNone(result)
        query1 = '''DELETE  FROM  users  WHERE user_name="user_test" '''
        cursor1 =self.conn.cursor()
        cursor1.execute(query1)
        self.conn.commit()
       
from unittest import TestCase
from dao.customerDao import *
from dao.customer import *

class TestCustomerDao(TestCase):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)
    
    def test_01_create_customer_ok(self):
        # GIVEN
        c_dao = CustomerDao()
        c1=customer("sony")
       # WHEN
        created=c_dao.add_customer(c1)
        # THEN
        self.assertTrue(created)
        self.assertIsNotNone(c1.id_customer)

    def test_02_find_customer_by_id_ok(self):
        c_dao = CustomerDao()
        c=c_dao.find_customer_by_id(2)
        if c:
           self.assertEqual(2,c.id_customer)
        else:
            print("aucun  customer a cet id ")
        cursor =self.conn.cursor()
        query = '''DELETE  FROM  customers  WHERE customer_name="sony"'''
        cursor.execute(query)
        self.conn.commit()

        

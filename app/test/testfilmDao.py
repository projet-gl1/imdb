from unittest import TestCase
from dao.filmDao import *
from pattern.filterByTitle import *
from pattern.filterByYear import *
import sqlite3

class TestfilmDao(TestCase):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)
    cursor =conn.cursor()
    query= '''INSERT INTO films VALUES ('id_test', 1, 'title_test', 'year_test', 5, 'crew_test', 1)'''
    cursor.execute(query)
    conn.commit() 
      
    def test_01_get_films_without_filter(self):
        film = filmDao()
        result = film.get_films()
        self.assertIsNotNone(result)
        self.assertGreater(len(result), 0)


    def test_02_find_film_by_title(self):
        film = filmDao()
        result = film.find_film_by_title("title_test")
        assert result is not None, "No film found with given title"
        assert len(result) == 1, "Multiple films found with same title"
        assert result[0]['title'] == "title_test", "Incorrect film returned"


    def test_03_find_film_by_title_empty(self):
        film = filmDao()
        result = film.find_film_by_title("fkqfncn")
        self.assertIsNone(result)


    def test_03_find_films_by_year(self):
        film = filmDao()
        result = film.find_films_by_year("year_test")
        #print(result)
        assert result is not None, "No film found in given year"
        for film in result:
            assert film['year'] == "year_test", "Incorrect year found in result"


    def test_04_find_films_by_year_empty(self):
        film = filmDao()
        result = film.find_films_by_year("2050")
        self.assertIsNone(result)


    def test_04_find_films_by_rating(self):
            film = filmDao()
            result = film.find_films_by_rating(5)
            assert result is not None, "No film found in given Rating"


    def test_05_find_films_by_rating_empty(self):
            film = filmDao()
            result = film.find_films_by_rating(112.0)
            assert result is None, "Film found in given Rating"
    def test_06_update_from_film(self):
        film = filmDao()
        result =film.update_film("id_test", 5, 6)

        self.assertTrue(result)

    def test_07_delete_from_film(self):
        film = filmDao()
        result = film.delete_film('id_test')

        self.assertTrue(result)

from unittest import TestCase
from dao.reservation import *


class Testreservations(TestCase):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)

    query='''INSERT INTO customers (customer_name) VALUES ('customer_test')'''
    cursor =conn.cursor()
    cursor.execute(query)
    conn.commit()
    customer_id = cursor.lastrowid

    def test_01_check_id_film_exists(self):
        cursor =self.conn.cursor()
        query= '''INSERT INTO films VALUES ('id_test', 1, 'title_test', 'year_test', 5, 'crew_test', 1)'''
        cursor.execute(query)
        self.conn.commit()
        reservation = reservationDao()
        result = reservation.check_id_film_exists("id_test")
        self.assertTrue(result)

    def test_02_reserve_film(self,customer_id=customer_id):
        reservation = reservationDao()
        result = reservation.reserve_film(customer_id,"id_test")
        self.assertTrue(result)
        
    def test_03_check_if_film_reserved_by_cutomer(self,customer_id=customer_id):
        reservation = reservationDao()
        result = reservation.check_if_film_reserved_by_customer(customer_id,"id_test")
        self.assertTrue(result)

    def test_04_return_film(self,customer_id=customer_id):
        reservation = reservationDao()
        result = reservation.return_film(customer_id,"id_test")
        self.assertTrue(result)
        cursor = self.conn.cursor()
        query1= '''DELETE  FROM  reservations  WHERE customer_id=? '''
        cursor.execute(query1, (customer_id,))
        self.conn.commit()
        query2='''DELETE  FROM  films WHERE id="id_test" '''
        cursor.execute(query2)
        self.conn.commit()        
    
    query='''DELETE  FROM  customers  WHERE customer_name="customer_test"'''
    cursor =conn.cursor()
    cursor.execute(query)
    conn.commit()

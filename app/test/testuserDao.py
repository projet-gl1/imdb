from unittest import TestCase
from dao.userdao import UserDao
from dao.user import user
import sqlite3

class TestUserDao(TestCase):

    conn = sqlite3.connect('imdb.db',check_same_thread=False)
    conn.commit()

    def test_01_create_user_ok(self):
        # GIVEN
        user_dao = UserDao()
        user1=user("sony","sony@gmail.com","Mariem","123")
        # WHEN
        created= user_dao.add_user(user1)
        # THEN
        self.assertTrue(created)
        self.assertIsNotNone(user1.id_user)
        #on supprime le user ajouté
        cursor = self.conn.cursor()
        query = '''DELETE  FROM  users  WHERE user_name="sony"'''
        cursor.execute(query)
        self.conn.commit()

    def test_02_find_user_by_id_ok(self):
        user_dao = UserDao()
        user=user_dao.find_user_by_id(1)
        self.assertEqual(1,user.id_user)

    def test_02_find_user_by_id_not_ok(self):
        user_dao = UserDao()
        user=user_dao.find_user_by_id(1)
        self.assertNotEqual(-1,user.id_user)
        
    def test_03_find_all_users_ok(self):
        user_dao =UserDao()
        list=user_dao.find_all_users()
        print(len(list))
   
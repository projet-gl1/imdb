import requests

def authenticate_user(username, password):

    url="http://127.0.0.1:5002/login"

    headers = {"Content-Type": "application/json"}
    payload = {"username": username, "password": password}

    # Send the GET request
    response = requests.post(url, headers=headers, json=payload)

    # Check the response status code
    if response.status_code == 200:
        token = response.json()['access_token']
        return token
    elif response.status_code == 401:
        print('Erreur d\'authentification')
        return None

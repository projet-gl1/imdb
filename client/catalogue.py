import requests

def display_catalogue(token):

    url="http://127.0.0.1:5002/films"

    # The access token
    headers = {"Authorization": "Bearer " + token}

    # Send the GET request
    response = requests.get(url, headers=headers)

    # Check the response status code
    if response.status_code == 200:
        try:
            all_movies = response.json()
        except:
            print('Aucun film disponible à la location.')
            return False
        else:
            print('Voci le catalogue disponible:')
            for movie in all_movies:
                if movie['available'] == 1:
                    title = movie['title']
                    id = movie['id']
                    year = movie['year']
                    rating = movie['rating']
                    print('{} | {} - {}, {}'.format(id, title, year, rating))
            return True
    else:
        print("Oups, le catalogue n'est pas disponible pour l'instant!")
        return False


def search_catalogue(token, search_type):

    if search_type == "year":
        filter = input('Veuillez saisir l\'année: ')
    elif search_type == "title":
        filter = input('Veuillez saisir le titre: ')
    elif search_type == "rating":
        filter = input("Veuillez saisir la note minimale: ")
        try:
            filter = float(filter)
        except:
            print("Erreur, vous devez saisir un chiffre.")

    url="http://127.0.0.1:5002/films"
    param = {search_type: filter}

    # The access token
    headers = {"Authorization": "Bearer " + token}

    # Send the GET request
    response = requests.get(url, headers=headers, params=param)

    # Check the response status code
    if response.status_code == 200:
        try:
            all_movies = response.json()
        except:
            print('Aucun film disponible ne répond au critère spécifié.')
        else:
            print('Voci les films disponibles et correspondants au critère:')
            for movie in all_movies:
                if movie['available'] == 1:
                    title = movie['title']
                    id = movie['id']
                    year = movie['year']
                    rating = movie['rating']
                    print('{} | {} - {}, {}'.format(id, title, year, rating))
    else:
        print("Oups, le catalogue n'est pas disponible pour l'instant!")

def modify_film(token, movie_id, rating, rank):

    # Verif type
    try:
        rating = float(rating)
        rank = float(rank)
    except:
        print("Erreur, la note et le classement doivent être des valeurs numériques!")
    else:
        url="http://127.0.0.1:5002/UPDATE"

        # The access token
        headers = {"Authorization": "Bearer " + token, "Content-Type": "application/json"}
        payload = {"movie_id": movie_id, "rating": rating, "rank": rank}

        # Send the PUT request
        response = requests.put(url, headers=headers, json=payload)

        # Check the response status code
        if response.status_code == 200:
            print('Modification effectuée avec succès!')
        elif response.status_code == 404:
            print("Oups, ce film n'existe pas!")
        else:
            print('Oups, une erreur s\'est produite!')


def delete_film(token, movie_id):

    url="http://127.0.0.1:5002/DELETE"

    # The access token
    headers = {"Authorization": "Bearer " + token, "Content-Type": "application/json"}
    payload = {"movie_id": movie_id}

    # Send the DELETE request
    response = requests.delete(url, headers=headers, json=payload)

    # Check the response status code
    if response.status_code == 200:
        print('Suppression effectuée avec succès!')
    elif response.status_code == 404:
        print("Oups, ce film n'existe pas!")
    else:
        print('Oups, une erreur s\'est produite!')


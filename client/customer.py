import requests

def add_customer(token, customer_name):

    url="http://127.0.0.1:5002/newcustomer"

    # The access token
    headers = {"Authorization": "Bearer " + token, "Content-Type": "application/json"}
    payload = {"customer_name": customer_name}

    # Send the GET request
    response = requests.post(url, headers=headers, json=payload)

    # Check the response status code
    if response.status_code == 200:
        print('Client enregistré avec succès!')
    elif response.status_code == 404:
        print("Oups, ce client est déjà enregistré!")
    else:
        print('Oups, une erreur s\'est produite!')
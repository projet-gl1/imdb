from catalogue import *
from authentication import *
from reservation import *
from customer import *

if __name__ == "__main__":

    print('Bienvenue dans notre application de gestion de vidéoclub!')
    token = None

    while token == None:
        user_name = input('Veuillez entrer votre nom d\'utilisateur: ')
        mdp = input('Veuillez entrer votre mot de passe: ')
        token = authenticate_user(user_name, mdp)

    choice = 0
    while choice != 8:
        print('''Veuillez saisir le numéro de l'action que vous souhaitez effectuer:
        1 - Consulter le catalogue disponible
        2 - Faire une recherche dans le catalogue (par année, par titre, par note)
        3 - Enregistrer une réservation
        4 - Enregistrer un retour
        5 - Modifier un film dans le catalogue
        6 - Supprimer un film du catalogue
        7 - Ajouter un nouveau client
        8 - Quitter
        ''')
        choice = input()
        try:
            choice = int(choice)
        except:
            print('Erreur, vous devez saisir un numéro.')
            choice = 0
        
        if choice == 1:
            display_catalogue(token)

        elif choice == 2:
            search_choice = 0
            search_keywords = {1: 'year', 2:'title', 3:'rating'}
            while search_choice not in [1, 2, 3]:
                print('''Veuillez saisir le numéro de l'action que vous souhaitez effectuer:
                1 - Rechercher par année
                2 - Rechercher par titre
                3 - Rechercher par note 
                ''')
                search_choice = input()
                try:
                    search_choice = int(search_choice)
                except:
                    print('Erreur, vous devez saisir un numéro.')
                    search_choice = 0
            search_catalogue(token, search_keywords[search_choice])

        elif choice == 3:
            customer_id = input('Veuillez entrer le nom du client: ')
            movie_id = input('Veuillez entrer l\'identifiant du film: ')
            make_reservation(token, customer_id, movie_id)

        elif choice == 4:
            customer_id = input('Veuillez entrer le nom du client: ')
            movie_id = input('Veuillez entrer l\'identifiant du film: ')
            return_reservation(token, customer_id, movie_id)

        elif choice == 5:
            movie_id = input("Veuillez entrer l'identifiant du film que vous souhaitez modifier: ")
            rating = input("Veuillez entrer la nouvelle note du film: ")
            rank = input("Veuillez entrer le nouveau classement du film: ")
            modify_film(token, movie_id, rating, rank)

        elif choice == 6:
            movie_id = input("Veuillez entrer l'identifiant du film que vous souhaitez supprimer du catalogue: ")
            delete_film(token, movie_id)

        elif choice == 7:
            customer_name = input('Veuillez entrer le nom du nouveau client: ')
            add_customer(token, customer_name)

        elif choice == 8:
            print('A bientôt!')
     

        # Close the server connection.

import requests

def make_reservation(token, customer_name, movie_id):

    url="http://127.0.0.1:5002/reserve"

    # The access token
    headers = {"Authorization": "Bearer " + token, "Content-Type": "application/json"}
    payload = {"customer_name": customer_name, "movie_id": movie_id}

    # Send the GET request
    response = requests.post(url, headers=headers, json=payload)

    # Check the response status code
    if response.status_code == 200:
        print('Réservation effectuée avec succès!')
    elif response.status_code == 404:
        print("Oups, cet utilisateur ou ce film n'existe pas!")
    else:
        print('Oups, une erreur s\'est produite!')


def return_reservation(token, customer_name, movie_id):

    url="http://127.0.0.1:5002/return"

    # The access token
    headers = {"Authorization": "Bearer " + token, "Content-Type": "application/json"}
    payload = {"customer_name": customer_name, "movie_id": movie_id}

    # Send the GET request
    response = requests.post(url, headers=headers, json=payload)

    # Check the response status code
    if response.status_code == 200:
        print('Retour effectué avec succès!')
    elif response.status_code == 404:
        print("Oups, cet utilisateur ou ce film n'existe pas!")
    else:
        print('Oups, une erreur s\'est produite!')
    